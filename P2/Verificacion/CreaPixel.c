#include <stdio.h>
#include <stdlib.h>

void escupeRGB(int alto, int ancho){

  printf("%d %d\n", alto, ancho);
  for(long int i=0; i<alto; i++){
    for(long int j=0; j<ancho-1; j++)
      printf("255 0 0 ");
    printf("255 0 0\n");
    for(long int j=0; j<ancho-1; j++)
      printf("0 255 0 ");
    printf("0 255 0\n");
    for(long int j=0; j<ancho-1; j++)
      printf("0 0 255 ");
    printf("0 0 255\n");
  }
}

int main(int argc, char *argv[]){

  if(argc!=3){
    fprintf(stderr, "Primer argumento, alto; segundo argumento, ancho.\n");
    exit(1);
  }

  char* aux=argv[1];
  char* comprobacion=NULL;

  int alto = (int) strtol(aux,&comprobacion,10);

	if(!( aux!= NULL && aux[0]!='\0' && comprobacion != NULL && comprobacion[0]=='\0')){
    fprintf(stderr, "El primer argumento no es un numero.\n");
    exit(1);
  }

  aux=argv[2];
  comprobacion=NULL;

  int ancho = (int) strtol(aux,&comprobacion,10);

	if(!( aux!= NULL && aux[0]!='\0' && comprobacion != NULL && comprobacion[0]=='\0')){
    fprintf(stderr, "El segundo argumento no es un numero.\n");
    exit(1);
  }

  escupeRGB(alto, ancho);

  return 0;
}
