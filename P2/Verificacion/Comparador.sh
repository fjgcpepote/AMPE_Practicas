#!/bin/bash

make -C .. >/dev/null

entrada=$(mktemp)
./CreaPixel $1 $2 > $entrada

correcto=$(mktemp)
../Secuencial_Escalar -p -f $entrada| head -n -2 > $correcto

sse=$(mktemp)
../Secuencial_SSE -p -f $entrada| head -n -2 > $sse

avx=$(mktemp)
../Secuencial_AVX -p -f $entrada| head -n -2 > $avx

openmp=$(mktemp)
../Paralelizado_AVX -p -f $entrada| head -n -2 > $openmp

diff $correcto $sse>/dev/null
diferencia=$?
if [ ! $diferencia -eq 0 ]
then
	echo "SSE es incorrecto"
fi

diff $correcto $avx>/dev/null
diferencia=$?
if [ ! $diferencia -eq 0 ]
then
	echo "AVX es incorrecto"
fi

diff $correcto $openmp>/dev/null
diferencia=$?
if [ ! $diferencia -eq 0 ]
then
	echo "OpenMP es incorrecto"
fi

rm -f $entrada $correcto $sse $avx
