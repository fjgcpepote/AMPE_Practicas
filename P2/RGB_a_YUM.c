#define _XOPEN_SOURCE
#define _POSIX_C_SOURCE 2

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h> //gettimeofday
#include <time.h> //clock

#define ALTO 3
#define ANCHO 3

extern char *optarg;
extern int optind, opterr, optopt;
extern void RGB_a_YUM(float* datos, int altoPixel, int anchoPixel);

float* leeEntrada(char* fichero, int* altoPixel, int* anchoPixel){

  FILE* stream;
  //Entrada estandar
  if(fichero==NULL) stream=fdopen(1,"r");
  //Fichero
  else stream=fopen(fichero,"r");

  if(stream==NULL){

    perror("ERROR abriendo stream de entrada");
    exit(1);
  }

  if(fscanf(stream, "%d",altoPixel)<0){

    fprintf(stderr, "ERROR leyendo la entrada\n");
    exit(1);
  }

  if(fscanf(stream, "%d",anchoPixel)<0){

    fprintf(stderr, "ERROR leyendo la entrada\n");
    exit(1);
  }

  float* datos=(float*)malloc((*altoPixel)*ALTO*(*anchoPixel)*ANCHO*sizeof(float));
  if(datos==NULL){

    perror("ERROR reservando memoria para la entrada");
    exit(1);
  }

  for(int i=0;i<*altoPixel*ALTO;i++)
    for(int j=0;j<*anchoPixel*ANCHO;j++)
      if(fscanf(stream, "%f",datos+i*(*anchoPixel)*ANCHO+j)<0){

        fprintf(stderr, "ERROR leyendo la entrada\n");
        exit(1);
      }
  return datos;
}

void imprimirMatriz(float* matriz, int altoPixel, int anchoPixel){

  int anchoTotal=anchoPixel*ANCHO;
  for(int i=0;i<altoPixel*ALTO;i++){
    for(int j=0;j<anchoTotal;j++)
      printf("%d ",(int)matriz[i*anchoTotal+j]);
    printf("\n");
  }
  printf("\n");
}

int main(int argc, char *argv[]){

int imprimir=0;
char* fichero=NULL;
char opt;

//Tratamos los argumentos, en caso de que haya
while ((opt = getopt(argc, argv, "f:p")) != -1) {

  switch (opt){
    case 'f':
      if(fichero != NULL){

        fprintf(stderr, "Uso ./%s [-f fichero] [-p]\n",argv[0]);
        exit(1);
      }
      fichero = optarg;
      break;
  case 'p':
      if(imprimir!=0){

        fprintf(stderr, "Uso ./%s [-f fichero] [-p]\n",argv[0]);
        exit(1);
      }
      imprimir=1;
      break;
  default:
      fprintf(stderr, "Uso ./%s [-f fichero] [-p]\n",argv[0]);
      exit(1);
    }
  }

  int* altoPixel = (int*) malloc(sizeof(int));
  if(altoPixel==NULL){

    perror("ERROR reservando memoria para la entrada");
    exit(1);
  }

  int* anchoPixel = (int*) malloc(sizeof(int));
  if(anchoPixel==NULL){

    perror("ERROR reservando memoria para la entrada");
    exit(1);
  }

  //Leemos de la entrada y reservamos memoria
  float* datos=leeEntrada(fichero, altoPixel, anchoPixel);

  if(imprimir){
    printf("Entrada:\n");
    imprimirMatriz(datos, *altoPixel, *anchoPixel);
  }

  //Toma de tiempo inicial
  struct timeval inicioGTOD, finGTOD;
  clock_t inicioC, finC;
  gettimeofday(&inicioGTOD, NULL);
  inicioC = clock();

  //Llamada a la transformación
  RGB_a_YUM(datos, *altoPixel, *anchoPixel);

  //Toma de tiempo final
  gettimeofday(&finGTOD, NULL);
  finC = clock();

  if(imprimir){
    printf("Salida:\n");
    imprimirMatriz(datos, *altoPixel, *anchoPixel);
  }

  //Liberamos memoria e imprimimos

  free(datos);

  double tiempoGTOD = ((double)finGTOD.tv_sec + (double)finGTOD.tv_usec/1000000) - ((double)inicioGTOD.tv_sec + (double)inicioGTOD.tv_usec/1000000);
  double tiempoC = ((double)(finC - inicioC))/(double)CLOCKS_PER_SEC;

  printf("Tiempo (gettimeofday) = %.9f''\n",tiempoGTOD);
  printf("Tiempo (clocks) = %.9f''\n",tiempoC);

  free(altoPixel);
  free(anchoPixel);
}
