#include "immintrin.h"
#include <stdlib.h>

#define ALTO 3
#define ANCHO 3
#define TRANSFORMACION_0 0.299
#define TRANSFORMACION_1 0.587
#define TRANSFORMACION_2 0.114
#define TRANSFORMACION_3 -0.147
#define TRANSFORMACION_4 -0.289
#define TRANSFORMACION_5 0.436
#define TRANSFORMACION_6 0.615
#define TRANSFORMACION_7 -0.515
#define TRANSFORMACION_8 -0.1

void RGB_a_YUM(float* datos, int altoPixel, int anchoPixel){

  //Carga de los valores constantes
  float mask[8]={0.0, 128.0, 128.0, 0.0, 0.0, 128.0, 128.0, 0.0};
  __m256 avx_mask = _mm256_load_ps(mask);

  float CR[8]={TRANSFORMACION_0, TRANSFORMACION_3, TRANSFORMACION_6, 0.0, TRANSFORMACION_0, TRANSFORMACION_3, TRANSFORMACION_6, 0.0};
  __m256 avx_CR = _mm256_load_ps(CR);

  float CG[8]={TRANSFORMACION_1, TRANSFORMACION_4, TRANSFORMACION_7, 0.0, TRANSFORMACION_1, TRANSFORMACION_4, TRANSFORMACION_7, 0.0};
  __m256 avx_CG = _mm256_load_ps(CG);

  float CB[8]={TRANSFORMACION_2, TRANSFORMACION_5, TRANSFORMACION_8, 0.0, TRANSFORMACION_2, TRANSFORMACION_5, TRANSFORMACION_8, 0.0};
  __m256 avx_CB = _mm256_load_ps(CB);

  __m256 avx_R, avx_G, avx_B, avx_Y, avx_U, avx_V;

  //Declaracion de arrays alineados para poder hacer streaming stores
  __attribute__((aligned(32))) float Y[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  __attribute__((aligned(32))) float U[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  __attribute__((aligned(32))) float V[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  int fila1p1, fila2p1, fila3p1, fila1p2, fila2p2, fila3p2;
  //Calculamos el ancho total de una fila para poder acceder directamente a la siguiente
  int anchoTotal=anchoPixel*ANCHO;

  //Comprobamos la paridad de filas y columnas
  int filasImpares=0;
  if(altoPixel%2){
    filasImpares++;
    altoPixel--;
  }
  int columnasImpares=0;
  if(anchoPixel%2){
    columnasImpares++;
    anchoPixel--;
  }

  //Caso general para imagenes con ancho y alto par
  for(int i=0;i<altoPixel*ALTO;i+=ALTO*2)
    for(int j=0;j<anchoTotal;j+=ANCHO){

      //Calculo del comienzo de cada fila (para ahorrar operaciones)
      fila1p1=i*anchoTotal+j;
      fila2p1=(i+1)*anchoTotal+j;
      fila3p1=(i+2)*anchoTotal+j;
      fila1p2=(i+3)*anchoTotal+j;
      fila2p2=(i+4)*anchoTotal+j;
      fila3p2=(i+5)*anchoTotal+j;

      //Difusion de las componentes
      //AVX no permite difusiones que ocupen el registro completo
      //Broadcast copia los datos a la parte baja de cada registro
      //Insert mueve el primer registro al registro destino
      //y la parte baja del segundo registro a la parte alta del destino
      avx_R = _mm256_insertf128_ps(_mm256_broadcast_ss(&datos[fila1p2]),_mm_broadcast_ss(&datos[fila1p1]),1);
      avx_G = _mm256_insertf128_ps(_mm256_broadcast_ss(&datos[fila2p2+1]),_mm_broadcast_ss(&datos[fila2p1+1]),1);
      avx_B = _mm256_insertf128_ps(_mm256_broadcast_ss(&datos[fila3p2+2]),_mm_broadcast_ss(&datos[fila3p1+2]),1);

      //Calculo de las componentes transformadas coeficiente * valor + mascara
      avx_Y = _mm256_add_ps(_mm256_mul_ps(avx_R, avx_CR), avx_mask);
      avx_U = _mm256_add_ps(_mm256_mul_ps(avx_G, avx_CG), avx_mask);
      avx_V = _mm256_add_ps(_mm256_mul_ps(avx_B, avx_CB), avx_mask);

      //Recuperacion de las componentes transformadas
      _mm256_store_ps(Y, avx_Y);
      _mm256_store_ps(U, avx_U);
      _mm256_store_ps(V, avx_V);

      //Ajustes finales
      if(Y[2]>255) Y[2]=255;
      if(U[2]<0) U[2]=0;
      if(V[2]<0) V[2]=0;
      if(Y[6]>255) Y[6]=255;
      if(U[6]<0) U[6]=0;
      if(V[6]<0) V[6]=0;

      //Almacenamiento de las componentes transformadas
      datos[fila1p1]=Y[0];
      datos[fila1p1+1]=Y[1];
      datos[fila1p1+2]=Y[2];
      datos[fila2p1]=U[0];
      datos[fila2p1+1]=U[1];
      datos[fila2p1+2]=U[2];
      datos[fila3p1]=V[0];
      datos[fila3p1+1]=V[1];
      datos[fila3p1+2]=V[2];
      datos[fila1p2]=Y[4];
      datos[fila1p2+1]=Y[5];
      datos[fila1p2+2]=Y[6];
      datos[fila2p2]=U[4];
      datos[fila2p2+1]=U[5];
      datos[fila2p2+2]=U[6];
      datos[fila3p2]=V[4];
      datos[fila3p2+1]=V[5];
      datos[fila3p2+2]=V[6];
  }

  //Ajuste para filas impares
  if(filasImpares){
    int inicio=altoPixel*ALTO*anchoTotal;
    for(int i=0;i<anchoPixel*ANCHO;i+=2*ANCHO){

      //Calculo del comienzo de cada fila (para ahorrar operaciones)
      fila1p1=inicio+i;
      fila2p1=inicio+anchoTotal+i;
      fila3p1=inicio+2*anchoTotal+i;
      fila1p2=inicio+ANCHO+i;
      fila2p2=inicio+anchoTotal+ANCHO+i;
      fila3p2=inicio+2*anchoTotal+ANCHO+i;

      //Difusion de las componentes
      //AVX no permite difusiones que ocupen el registro completo
      //Broadcast copia los datos a la parte baja de cada registro
      //Insert mueve el primer registro al registro destino
      //y la parte baja del segundo registro a la parte alta del destino
      avx_R = _mm256_insertf128_ps(_mm256_broadcast_ss(&datos[fila1p2]),_mm_broadcast_ss(&datos[fila1p1]),1);
      avx_G = _mm256_insertf128_ps(_mm256_broadcast_ss(&datos[fila2p2+1]),_mm_broadcast_ss(&datos[fila2p1+1]),1);
      avx_B = _mm256_insertf128_ps(_mm256_broadcast_ss(&datos[fila3p2+2]),_mm_broadcast_ss(&datos[fila3p1+2]),1);

      //Calculo de las componentes transformadas coeficiente * valor + mascara
      avx_Y = _mm256_add_ps(_mm256_mul_ps(avx_R, avx_CR), avx_mask);
      avx_U = _mm256_add_ps(_mm256_mul_ps(avx_G, avx_CG), avx_mask);
      avx_V = _mm256_add_ps(_mm256_mul_ps(avx_B, avx_CB), avx_mask);

      //Recuperacion de las componentes transformadas
      _mm256_store_ps(Y, avx_Y);
      _mm256_store_ps(U, avx_U);
      _mm256_store_ps(V, avx_V);

      //Ajustes finales
      if(Y[2]>255) Y[2]=255;
      if(U[2]<0) U[2]=0;
      if(V[2]<0) V[2]=0;
      if(Y[6]>255) Y[6]=255;
      if(U[6]<0) U[6]=0;
      if(V[6]<0) V[6]=0;

      //Almacenamiento de las componentes transformadas
      datos[fila1p1]=Y[0];
      datos[fila1p1+1]=Y[1];
      datos[fila1p1+2]=Y[2];
      datos[fila2p1]=U[0];
      datos[fila2p1+1]=U[1];
      datos[fila2p1+2]=U[2];
      datos[fila3p1]=V[0];
      datos[fila3p1+1]=V[1];
      datos[fila3p1+2]=V[2];
      datos[fila1p2]=Y[4];
      datos[fila1p2+1]=Y[5];
      datos[fila1p2+2]=Y[6];
      datos[fila2p2]=U[4];
      datos[fila2p2+1]=U[5];
      datos[fila2p2+2]=U[6];
      datos[fila3p2]=V[4];
      datos[fila3p2+1]=V[5];
      datos[fila3p2+2]=V[6];
    }
    //Ajuste para filas impares y columnas impares
    if(columnasImpares){

      inicio+=anchoPixel*ANCHO;

      //Calculo del comienzo de cada fila (para ahorrar operaciones)
      fila1p1=inicio;
      fila2p1=inicio+anchoTotal;
      fila3p1=inicio+2*anchoTotal;

      //Como solo se transforma un pixel, basta con usar la parte baja del registro
      avx_R = _mm256_broadcast_ss(&datos[fila1p1]);
      avx_G = _mm256_broadcast_ss(&datos[fila2p1+1]);
      avx_B = _mm256_broadcast_ss(&datos[fila3p1+2]);

      //Calculo de las componentes transformadas coeficiente * valor + mascara
      avx_Y = _mm256_add_ps(_mm256_mul_ps(avx_R, avx_CR), avx_mask);
      avx_U = _mm256_add_ps(_mm256_mul_ps(avx_G, avx_CG), avx_mask);
      avx_V = _mm256_add_ps(_mm256_mul_ps(avx_B, avx_CB), avx_mask);

      //Recuperacion de las componentes transformadas
      _mm256_store_ps(Y, avx_Y);
      _mm256_store_ps(U, avx_U);
      _mm256_store_ps(V, avx_V);

      //Ajustes finales (como en la version escalar o SSE)
      if(Y[2]>255) Y[2]=255;
      if(U[2]<0) U[2]=0;
      if(V[2]<0) V[2]=0;

      //Almacenamiento de las componentes transformadas (como en la version escalar o SSE)
      datos[fila1p1]=Y[0];
      datos[fila1p1+1]=Y[1];
      datos[fila1p1+2]=Y[2];
      datos[fila2p1]=U[0];
      datos[fila2p1+1]=U[1];
      datos[fila2p1+2]=U[2];
      datos[fila3p1]=V[0];
      datos[fila3p1+1]=V[1];
      datos[fila3p1+2]=V[2];
    }
  }
}
