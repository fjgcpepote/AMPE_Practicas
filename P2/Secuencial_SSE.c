#include "xmmintrin.h"

#define ALTO 3
#define ANCHO 3
#define TRANSFORMACION_0 0.299
#define TRANSFORMACION_1 0.587
#define TRANSFORMACION_2 0.114
#define TRANSFORMACION_3 -0.147
#define TRANSFORMACION_4 -0.289
#define TRANSFORMACION_5 0.436
#define TRANSFORMACION_6 0.615
#define TRANSFORMACION_7 -0.515
#define TRANSFORMACION_8 -0.1

void RGB_a_YUM(float* datos, int altoPixel, int anchoPixel){

  //Carga de los valores constantes
  float mask[4]={0.0, 128.0, 128.0, 0.0};
  __m128 sse_mask = _mm_load_ps(mask);

  float CR[4]={TRANSFORMACION_0, TRANSFORMACION_3, TRANSFORMACION_6, 0.0};
  __m128 sse_CR = _mm_load_ps(CR);

  float CG[4]={TRANSFORMACION_1, TRANSFORMACION_4, TRANSFORMACION_7, 0.0};
  __m128 sse_CG = _mm_load_ps(CG);

  float CB[4]={TRANSFORMACION_2, TRANSFORMACION_5, TRANSFORMACION_8, 0.0};
  __m128 sse_CB = _mm_load_ps(CB);

  __m128 sse_R, sse_G, sse_B, sse_Y, sse_U, sse_V;

  //Declaracion de arrays alineados para poder hacer streaming stores
  __attribute__((aligned(16))) float Y[4] = {0.0, 0.0, 0.0, 0.0};
  __attribute__((aligned(16))) float U[4] = {0.0, 0.0, 0.0, 0.0};
  __attribute__((aligned(16))) float V[4] = {0.0, 0.0, 0.0, 0.0};

  int fila1, fila2, fila3;
  //Calculamos el ancho total de una fila para poder acceder directamente a la siguiente
  int anchoTotal=anchoPixel*ANCHO;

  for(int i=0;i<altoPixel*ALTO;i+=ALTO)
    for(int j=0;j<anchoTotal;j+=ANCHO){

      //Calculo del comienzo de cada fila (para ahorrar operaciones)
      fila1=i*anchoTotal+j;
      fila2=(i+1)*anchoTotal+j;
      fila3=(i+2)*anchoTotal+j;

      //Difusion de las componentes
      sse_R = _mm_load1_ps(&datos[fila1]);
      sse_G = _mm_load1_ps(&datos[fila2+1]);
      sse_B = _mm_load1_ps(&datos[fila3+2]);

      //Calculo de las componentes transformadas coeficiente * valor + mascara
      sse_Y = _mm_add_ps(_mm_mul_ps(sse_R, sse_CR), sse_mask);
      sse_U = _mm_add_ps(_mm_mul_ps(sse_G, sse_CG), sse_mask);
      sse_V = _mm_add_ps(_mm_mul_ps(sse_B, sse_CB), sse_mask);

      //Recuperacion de las componentes transformadas
      _mm_store_ps(Y, sse_Y);
      _mm_store_ps(U, sse_U);
      _mm_store_ps(V, sse_V);

      //Ajustes finales
      if(Y[2]>255) Y[2]=255;
      if(U[2]<0) U[2]=0;
      if(V[2]<0) V[2]=0;

      //Almacenamiento de las componentes transformadas
      datos[fila1]=Y[0];
      datos[fila1+1]=Y[1];
      datos[fila1+2]=Y[2];
      datos[fila2]=U[0];
      datos[fila2+1]=U[1];
      datos[fila2+2]=U[2];
      datos[fila3]=V[0];
      datos[fila3+1]=V[1];
      datos[fila3+2]=V[2];
  }
}
