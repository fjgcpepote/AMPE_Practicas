#define ALTO 3
#define ANCHO 3
#define TRANSFORMACION_0 0.299
#define TRANSFORMACION_1 0.587
#define TRANSFORMACION_2 0.114
#define TRANSFORMACION_3 -0.147
#define TRANSFORMACION_4 -0.289
#define TRANSFORMACION_5 0.436
#define TRANSFORMACION_6 0.615
#define TRANSFORMACION_7 -0.515
#define TRANSFORMACION_8 -0.1

void RGB_a_YUM(float* datos, int altoPixel, int anchoPixel){

  int fila1, fila2, fila3;
  //Calculamos el ancho total de una fila para poder acceder directamente a la siguiente
  int anchoTotal=anchoPixel*ANCHO;
  for(int i=0;i<altoPixel*ALTO;i+=ALTO)
    for(int j=0;j<anchoTotal;j+=ANCHO){
      //Calculo del comienzo de cada fila (para ahorrar operaciones)
      fila1=i*anchoTotal+j;
      fila2=(i+1)*anchoTotal+j;
      fila3=(i+2)*anchoTotal+j;

      //Calculo de cada componente
      datos[fila2]=datos[fila2+1]*TRANSFORMACION_1;
      datos[fila3]=datos[fila3+2]*TRANSFORMACION_2;

      datos[fila1+1]=datos[fila1]*TRANSFORMACION_3+128;
      datos[fila3+1]=datos[fila3+2]*TRANSFORMACION_5+128;

      datos[fila1+2]=datos[fila1]*TRANSFORMACION_6+128;
      datos[fila2+2]=datos[fila2+1]*TRANSFORMACION_7+128;

      datos[fila1]=datos[fila1]*TRANSFORMACION_0;
      datos[fila2+1]=datos[fila2+1]*TRANSFORMACION_4+128;
      datos[fila3+2]=datos[fila3+2]*TRANSFORMACION_8+128;

      //Ajustes finales
      if(datos[fila1+2]>255) datos[fila1+2]=255;
      if(datos[fila2+2]<0) datos[fila2+2]=0;
      if(datos[fila3+2]<0) datos[fila3+2]=0;
    }
}
