void recontar(int* resultados, int* recuento, int numMesas, int numPartidos, int trasponer){

  int acumulador;

  //Comprobamos la trasposicion para sumar los valores adecuadamente
  if(trasponer)
    for(int i=0; i<numPartidos; i++){
      acumulador=0;
      for(int j=0; j<numMesas; j++)
        acumulador+=resultados[i*numMesas+j];
        recuento[i]=acumulador;
      }
  else
    for(int i=0; i<numPartidos; i++){
      acumulador=0;
      for(int j=0; j<numMesas; j++)
        acumulador+=resultados[i+j*numPartidos];
      recuento[i]=acumulador;
    }
}
