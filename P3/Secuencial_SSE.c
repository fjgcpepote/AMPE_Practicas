#include <emmintrin.h> //SSE2
#include <pmmintrin.h> //SSE3

void reconteoTraspuesto(int* resultados, int* recuento, int numMesas, int numPartidos){

  __m128i sum0;
  __m128i sum1;
  //Declaramos el array intermedio de forma alineada para permitir streaming stores
  __attribute__((aligned(16))) int cumsum[4];
  int j, sumaFinal;
  //Comprobamos el numero de operaciones que tendremos que hacer escalarmente
  int numMesas_mult4 = numMesas-numMesas%4;

  for(int i=0; i<numPartidos; i++){

    //Descartamos posibles datos de una iteracion anterior
    sumaFinal=0;

    if(numMesas_mult4>=4){

      //Cargamos los primeros cuatro valores
      sum0 = _mm_lddqu_si128((__m128i *) &resultados[i*numMesas]);

      for(j=4; j<numMesas_mult4; j+=4){
        //Vamos cargando valores de cuatro en cuatro y sumandolos
        sum1 = _mm_lddqu_si128((__m128i *) &resultados[i*numMesas+j]);
        sum0 = _mm_add_epi32(sum0, sum1);
      }

      //Recuperamos los valores
      _mm_stream_si128((__m128i *) cumsum, sum0);

      //Almacenamos los valores
      sumaFinal+=cumsum[0];
      sumaFinal+=cumsum[1];
      sumaFinal+=cumsum[2];
      sumaFinal+=cumsum[3];
    }

    //Ajustes finales para numMesas no multiplos de anchura de registros
    for(j=numMesas_mult4; j<numMesas; j++)
      sumaFinal+=resultados[i*numMesas+j];

    //Asignacion final
    recuento[i]=sumaFinal;
  }
}

void reconteo(int* resultados, int* recuento, int numMesas, int numPartidos){

  __m128i sum0;
  __m128i sum1;
  //Declaramos el array intermedio de forma alineada para permitir streaming stores
  __attribute__((aligned(16))) int cumsum[4];
  //Comprobamos el numero de operaciones que tendremos que hacer escalarmente
  int numPartidos_mult4 = numPartidos-numPartidos%4;
  int i, acumulador;
  for(i=0; i<numPartidos_mult4; i+=4){

    //Cargamos los primeros cuatro valores
    sum0 = _mm_lddqu_si128((__m128i *) &resultados[i]);

    for(int j=1; j<numMesas; j++){
      //Vamos cargando valores de cuatro en cuatro y sumandolos
      sum1 = _mm_lddqu_si128((__m128i *) &resultados[i+j*numPartidos]);
      sum0 = _mm_add_epi32(sum0, sum1);
    }

    //Recuperamos los valores
    _mm_stream_si128((__m128i *) cumsum, sum0);

    //Almacenamos los valores
    recuento[i]=cumsum[0];
    recuento[i+1]=cumsum[1];
    recuento[i+2]=cumsum[2];
    recuento[i+3]=cumsum[3];
  }

  //Ajuste final para numPartidos no multiplos de anchura de registros
  for(; i<numPartidos; i++){
    acumulador=0;
    for(int j=0; j<numMesas; j++)
      acumulador+=resultados[i+j*numPartidos];
    recuento[i]=acumulador;
  }
}

void recontar(int* resultados, int* recuento, int numMesas, int numPartidos, int trasponer){

  //Comprobamos la trasposicion para sumar los valores adecuadamente
  if(trasponer) reconteoTraspuesto(resultados, recuento, numMesas, numPartidos);
  else reconteo(resultados, recuento, numMesas, numPartidos);
}
