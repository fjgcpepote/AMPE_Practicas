#!/bin/bash

make -C .. >/dev/null

entrada=$(mktemp)
echo -e "$1\n$2\nS" > $entrada

correcto=$(mktemp)
../Secuencial_Escalar < $entrada | head -n -2 > $correcto

sse=$(mktemp)
../Secuencial_SSE < $entrada | head -n -2 > $sse

avx=$(mktemp)
../Secuencial_AVX < $entrada | head -n -2 > $avx

openmp=$(mktemp)
../Paralelizado_AVX < $entrada | head -n -2 > $openmp

diff $correcto $sse>/dev/null
diferencia=$?
if [ ! $diferencia -eq 0 ]
then
	echo "SSE es incorrecto"
fi

diff $correcto $avx>/dev/null
diferencia=$?
if [ ! $diferencia -eq 0 ]
then
	echo "AVX es incorrecto"
fi

diff $correcto $openmp>/dev/null
diferencia=$?
if [ ! $diferencia -eq 0 ]
then
	echo "OpenMP es incorrecto"
fi

rm -f $entrada $correcto $sse $avx $openmp
