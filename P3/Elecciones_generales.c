#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h> //gettimeofday
#include <time.h> //clock

#define TOTAL_MAXIMO 35000000
#define RANGO_MAXIMO 100000 //El maximo de votos para un partido en una mesa

void imprimeResultados(int* resultados, int* recuento, int numMesas, int numPartidos, int trasponer){

  printf("Resultados:\n");

  //Tenemos en cuenta como se han almacenado los datos de cara a su impresion
  if(trasponer)
    for(int i=0; i<numMesas; i++){
      printf("\t\t");
      for(int j=0; j<numPartidos-1; j++)
        printf("%d\t",resultados[i+j*numMesas]);
        printf("%d\n",resultados[i+(numPartidos-1)*numMesas]);
      }
  else
    for(int i=0; i<numMesas; i++){
      printf("\t\t");
      for(int j=0; j<numPartidos-1; j++)
        printf("%d\t",resultados[i*numPartidos+j]);
        printf("%d\n",resultados[i*numPartidos+numPartidos-1]);
    }

  printf("TOTAL:\t\t");
  for(int i=0; i<numPartidos; i++)
    printf("%d\t", recuento[i]);
  printf("\n");
}

void generarResultados(int* resultados, int numMesas, int numPartidos){

  int aux, acumulador=0, restantes=numMesas*numPartidos;
  srand(time(NULL));

  //Dado que la inicializacion es aleatoria y usamos un array unidimensional
  //podemos ignorar si trasponemos o no
  for(int i=0; i<numPartidos*numMesas; i++, restantes--){
    aux=rand()%RANGO_MAXIMO+1;
    //Si el valor generado impide que el resto de valores sean estrictamente
    //positivos, acotamos el valor al maximo posible
    //(El resto de datos tendran valor 1)
    if(aux+acumulador+restantes > TOTAL_MAXIMO)
      aux=TOTAL_MAXIMO-acumulador-restantes+1;
    acumulador+=aux;
    resultados[i]=aux;
    }
}

extern void recontar(int* resultados, int* recuento, int numMesas, int numPartidos, int trasponer);

int main(int argc, char *argv[]){

  int numMesas, numPartidos, leidoCorrectamente=0, trasponer=0;
  char mostrar;

  //Leemos de la entrada estandar
  while(!leidoCorrectamente){
    printf("Introduzca el numero de mesas electorales: ");
    if(scanf("%d", &numMesas)==1 && getchar() == '\n')
      leidoCorrectamente++;
    else {
      fprintf(stderr, "ERROR leyendo de la entrada estandar, se esperaba un numero\n");
      while(getchar() != '\n');
    }
  }

  leidoCorrectamente--;

  while(!leidoCorrectamente){
    printf("Introduzca el numero de partidos politicos: ");
    if(scanf("%d", &numPartidos)==1 && getchar() == '\n')
      leidoCorrectamente++;
    else {
      fprintf(stderr, "ERROR leyendo de la entrada estandar, se esperaba un numero\n");
      while(getchar() != '\n');
    }
  }

  //Comprobamos si el problema es satisfacible.
  //En caso de inclumpir la condicion, no todas las mesas podran
  //tener al menos un voto a cada partido
  if(numMesas*numPartidos > TOTAL_MAXIMO){
    fprintf(stderr, "El censo contiene %d habitantes, el numero de mesas por el numero de partidos lo excede (%d)\n",
     TOTAL_MAXIMO, numMesas*numPartidos);
    exit(1);
  }

  leidoCorrectamente--;

  while(!leidoCorrectamente){
    printf("Mostrar datos por pantalla (S/N): ");
    if(scanf("%c", &mostrar)==1 && (mostrar=='S' || mostrar=='N') && getchar() == '\n')
      leidoCorrectamente++;
    else {
      fprintf(stderr, "ERROR leyendo de la entrada estandar, se esperaba un caracter S o N\n");
      if(mostrar!='\n')
        while(getchar() != '\n');
    }
  }

  //Reservamos memoria para las estructuras de datos
  int* resultados=(int*) malloc(numMesas*numPartidos*sizeof(int));
  if(resultados==NULL && numMesas!=0 && numPartidos!=0){
    fprintf(stderr, "ERROR reservando memoria\n");
    exit(1);
  }

  int* recuento=(int*) malloc(numPartidos*sizeof(int));
  if(recuento==NULL && numPartidos!=0){
    fprintf(stderr, "ERROR reservando memoria\n");
    exit(1);
  }

  //Comprobamos si interesa trasponer
  if(numMesas>numPartidos) trasponer++;

  //Generamos valores
  generarResultados(resultados, numMesas, numPartidos);

  //Toma de tiempo inicial
  struct timeval inicioGTOD, finGTOD;
  clock_t inicioC, finC;
  gettimeofday(&inicioGTOD, NULL);
  inicioC = clock();

  //Llamamos a la funcion externa
  recontar(resultados, recuento, numMesas, numPartidos, trasponer);

  //Toma de tiempo final
  gettimeofday(&finGTOD, NULL);
  finC = clock();

  if(mostrar=='S') imprimeResultados(resultados, recuento, numMesas, numPartidos, trasponer);

  //Liberamos memoria e imprimimos
  free(resultados);
  free(recuento);

  double tiempoGTOD = ((double)finGTOD.tv_sec + (double)finGTOD.tv_usec/1000000) - ((double)inicioGTOD.tv_sec + (double)inicioGTOD.tv_usec/1000000);
  double tiempoC = ((double)(finC - inicioC))/(double)CLOCKS_PER_SEC;

  printf("Tiempo (gettimeofday) = %.9f''\n",tiempoGTOD);
  printf("Tiempo (clocks) = %.9f''\n",tiempoC);
}
