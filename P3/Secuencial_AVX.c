#include <immintrin.h> //AVX y AVX2

void reconteoTraspuesto(int* resultados, int* recuento, int numMesas, int numPartidos){

  __m256i sum0;
  __m256i sum1;
  //Declaramos el array intermedio de forma alineada para permitir streaming stores
  __attribute__((aligned(32))) int cumsum[8];
  //Comprobamos el numero de operaciones que tendremos que hacer escalarmente
  int numMesas_mult8 = numMesas-numMesas%8;
  int sumaFinal;

  for(int i=0; i<numPartidos; i++){

    //Descartamos posibles datos de una iteracion anterior
    sumaFinal=0;

    if(numMesas_mult8>=8){

      //Cargamos los primeros ocho valores
      sum0 = _mm256_lddqu_si256((__m256i *) &resultados[i*numMesas]);

      for(int j=8; j<numMesas_mult8; j+=8){
        //Vamos cargando valores de ocho en ocho y sumandolos
        sum1 = _mm256_lddqu_si256((__m256i *) &resultados[i*numMesas+j]);
        sum0 = _mm256_add_epi32(sum0, sum1);
      }

      //Recuperamos los valores
      _mm256_stream_si256((__m256i *) cumsum, sum0);

      //Almacenamos los valores
      sumaFinal+=cumsum[0];
      sumaFinal+=cumsum[1];
      sumaFinal+=cumsum[2];
      sumaFinal+=cumsum[3];
      sumaFinal+=cumsum[4];
      sumaFinal+=cumsum[5];
      sumaFinal+=cumsum[6];
      sumaFinal+=cumsum[7];
    }

    //Ajustes finales para numMesas no multiplos de anchura de registros
    for(int j=numMesas_mult8; j<numMesas; j++)
      sumaFinal+=resultados[i*numMesas+j];

    //Asignacion final
    recuento[i]=sumaFinal;
  }
}

void reconteo(int* resultados, int* recuento, int numMesas, int numPartidos){

  __m256i sum0;
  __m256i sum1;
  //Declaramos el array intermedio de forma alineada para permitir streaming stores
  __attribute__((aligned(32))) int cumsum[8];
  //Comprobamos el numero de operaciones que tendremos que hacer escalarmente
  int numPartidos_mult8 = numPartidos-numPartidos%8;
  int i, acumulador;

  for(i=0; i<numPartidos_mult8; i+=8){

    //Cargamos los primeros ocho valores
    sum0 = _mm256_lddqu_si256((__m256i *) &resultados[i]);

    for(int j=1; j<numMesas; j++){
      //Vamos cargando valores de ocho en ocho y sumandolos
      sum1 = _mm256_lddqu_si256((__m256i *) &resultados[i+j*numPartidos]);
      sum0 = _mm256_add_epi32(sum0, sum1);
    }

    //Recuperamos los valores
    _mm256_stream_si256((__m256i *) cumsum, sum0);

    //Almacenamos los valores
    recuento[i]=cumsum[0];
    recuento[i+1]=cumsum[1];
    recuento[i+2]=cumsum[2];
    recuento[i+3]=cumsum[3];
    recuento[i+4]=cumsum[4];
    recuento[i+5]=cumsum[5];
    recuento[i+6]=cumsum[6];
    recuento[i+7]=cumsum[7];
  }

  //Ajuste final para numPartidos no multiplos de anchura de registros
  for(; i<numPartidos;i++){
    acumulador=0;
    for(int j=0; j<numMesas; j++)
      acumulador+=resultados[i+j*numPartidos];
    recuento[i]=acumulador;
  }
}

void recontar(int* resultados, int* recuento, int numMesas, int numPartidos, int trasponer){

  //Comprobamos la trasposicion para sumar los valores adecuadamente
  if(trasponer) reconteoTraspuesto(resultados, recuento, numMesas, numPartidos);
  else reconteo(resultados, recuento, numMesas, numPartidos);
}
