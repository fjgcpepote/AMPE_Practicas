﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <xmmintrin.h>
#include <fvec.h>
#include <omp.h>

/* Transformada de Wawelet 1D con Daub-4: Version paralela. */
// Interpretacion de los posibles argumentos que pudiera haber en la invocacion
// al programa.

int interpretarArgumentos(int argc, char *argv[], int *n);
///Variable para saber si hay que mostrar los resultados por pantalla, por defecto a 0
int static MOSTRAR = 0;
// Calculo de la transformada.
void wt_4d(float *vector, int longVector);
void wt_4d_SSE(float *a, int n, F32vec4 coef0, F32vec4 coef1, F32vec4 coef2, F32vec4 coef3);
void calcular_coeficientes_all(F32vec4 vec0, F32vec4 vec1, F32vec4 vec2, F32vec4 vec3,
                                      F32vec4 coef0, F32vec4 coef1, F32vec4 coef2, F32vec4 coef3,
                                      F32vec4 *vec_4bajos, F32vec4 *vec_4altos);




// Funcion principal.
int main(int argc, char *argv[])
{
int longVector = 10000000, n, respExe = 0, lecturaArgumentos, i;
float *vector, parte_entera, parte_decimal;
struct timeval iteInicio, iteFinal;
double tiempo;

  F32vec4 coef0(0.4829629131445341);
  F32vec4 coef1(0.8365163037378079);
  F32vec4 coef2(0.2241438680420134);
  F32vec4 coef3(-0.1294095225512604);   // Cada valor del tipo "F32vec4"
                                        // contiene 4 floats empaquetados.
                                        // El constructor equivale al intrinseco
                                        // _mm_set1_ps sobre un __m128


// Interpretación de argumentos (si los hubiera).
lecturaArgumentos = interpretarArgumentos(argc, argv, &n);
if (lecturaArgumentos) {
  if (lecturaArgumentos == 2) { // --help
      printf("Uso: ./wt [--n=<tamaño_vector>] [true|false]\n");
      printf("Si se indica --n=<tamaño_vector>, el tamaño ha de ser >= 4.\n");
      printf("Si se indica true, se mostrarán por pantalla los valores.");
      printf("Si no se indica --n=<tamaño_vector>, el tamaño será %d y no se mostrarán los valores por pantalla.\n",longVector);
      printf("Ejemplo 1: ./wt\n");
      printf("Ejemplo 2: ./wt --n=500000 true\n");
  }
  // La sintaxis de la llamada es correcta, y se puede calcular la transformada.
  else {
    if (lecturaArgumentos >= 3) longVector = n; // Tomar valor "n".
    if(lecturaArgumentos == 4) MOSTRAR = 1;

    // Inicializacion del vector.
    vector = (float*)malloc(sizeof(float)*longVector);
        for (i=0; i<longVector; i++) {
      parte_entera = (float)(rand() % 255);
      parte_decimal = (float)(1 / ((float)(rand() % 100)));
      vector[i] = parte_entera + parte_decimal; // 0 <= vector[i] < 255
    }
    // ¿Mostramos vector de entrada?
    if (MOSTRAR)
    {
		printf("ENTRADA:");
		for (i=0; i<longVector; i++) printf("%8.2f ", vector[i]);
	}

    // Cálculo de la transformada.
    gettimeofday(&iteInicio, NULL);
    #ifdef ESCALAR
    wt_4d(vector, longVector);
    #else
    wt_4d_SSE(vector, longVector, coef0, coef1, coef2, coef3);
    #endif
    gettimeofday(&iteFinal, NULL);
    tiempo = ((double)iteFinal.tv_sec + (double)iteFinal.tv_usec/1000000) -
      ((double)iteInicio.tv_sec + (double)iteInicio.tv_usec/1000000);
    // ¿Mostramos vector de salida?
    if (MOSTRAR)
    {
		printf("\nSALIDA :"); for (i=0; i<longVector; i++) printf("%8.2f ", vector[i]);
		printf("\n");
	}
    // Liberación de recursos.
    free(vector);
    // Exhibición del tiempo consumido.
    printf("Tiempo = %6.4f''\n",tiempo);
  }
}
// Error sintáctico.
else {
  printf("Sintaxis incorrecta. Teclee ./wt --help\n");
  respExe = 1;
}

return respExe;

}

// Interpretación de los posibles argumentos que pudiere haber en la invocación
// al programa.
// - Entradas:
// argc: Número de argumentos de entrada.
// argv: Puntero a los argumentos de entrada.
// n: Puntero a una variable en donde se dejará la longitud del vector al que
// se le calculará la Transformada de Wavelet.
// - Salidas:
// 0 -> Sintaxis errónea. 1 -> Sin argumentos. 2 -> Argumento "--help".
// 3 -> Argumento "--n="; en este caso, el valor indicado se dejará sobre "n".
// 4-> Argumento true con "--n"
// 5-> Argumento false con "--n"
int interpretarArgumentos(int argc, char *argv[], int *n){


	int resp = (argc == 1 || argc == 2 || argc == 3); // Si el programa no tiene argumentos (argc=1)
	if (resp && argc == 2){ // o si tiene uno solo (argc=2), todo ok.
		// Hay un argumento. Éste podrá ser "--help" o "--n=<tamaño_vector>".
		if (strcmp(argv[1],"--help") == 0) resp = 2;
		else if (strncmp(argv[1],"--n=",4) == 0){
			*n = atoi(&argv[1][4]);
			if (*n >= 4) resp = 3;
			else resp = 0;
		}
		else resp = 0;
	}
	if(resp && argc == 3)
	{
		if (strncmp(argv[1],"--n=",4) == 0){
			*n = atoi(&argv[1][4]);
			if (*n >= 4)
			{
				if(strcmp(argv[2], "true") == 0)
					resp=4;
				else if(strcmp(argv[2], "false") == 0)
					resp = 5;
				else resp=0;
			}
			else resp = 0;

		}
		else resp = 0;

	}

	return resp;
}

// Cálculo de la transformada.
void wt_4d(float *vector,int longVector)
{
// Coeficientes wavelets Daub-4.
float c0 = 0.4829629131445341;
float c1 = 0.8365163037378079;
float c2 = 0.2241438680420134;
float c3 = -0.1294095225512604;
// Otras variables.
int i, mitad;
float *tmp;

// Control inicial: El vector de entrada debe contar con, al menos, 4 elementos.
if (longVector >= 4) {
	tmp = (float *) malloc(sizeof(float)*longVector); // Reserva del espacio necesario.
	// Bucle wavelet.
	mitad = longVector / 2;
  //No hay que declarar ninguna variable como privada, puesto que solo se escribe en el vector temp
  //y forzosamente se va a acceder a posiciones distintas
  #pragma omp parallel for num_threads(omp_get_max_threads())
	for (i = 0; i < longVector - 3; i += 2) {
		tmp[i/2] = c0 * vector[i] + c1 * vector[i+1] + c2 * vector[i+2] + c3 * vector[i+3];
		tmp[i/2+mitad] = c3 * vector[i] - c2 * vector[i+1] + c1 * vector[i+2] - c0 * vector[i+3];
	}
	// Ajustes finales.
	tmp[i/2] = c0 * vector[longVector-2] + c1 * vector[longVector-1] + c2 * vector[0] + c3 * vector[1];
	tmp[i/2+mitad] = c3 * vector[longVector-2] - c2 * vector[longVector-1] + c1 * vector[0] - c0 * vector[1];
	// Paso de los resultados, del vector temporal al vector de entrada/salida.
	for (i = 0; i < longVector; i++) vector[i] = tmp[i];
	free(tmp); // Liberación del espacio ya utilizado.
	}
}

// Obtención, a partir de un total de 16 floats de entrada (4 vectores de 4 floats),
// de los 8 floats "transformados" asociados.
// - Entradas:
//   vec0, ..., vec3:
//   coef0, ..., coef3: Coeficientes Daub-4.
// - Salidas:
//   vec_4bajos: 4 floats de la parte baja del array ya transformados.
//   vec_4altos: 4 floats de la parte alta del array ya transformados.
inline void calcular_coeficientes_all(F32vec4 vec0, F32vec4 vec1, F32vec4 vec2, F32vec4 vec3,
                                      F32vec4 coef0, F32vec4 coef1, F32vec4 coef2, F32vec4 coef3,
                                      F32vec4 *vec_4bajos, F32vec4 *vec_4altos)
{
  //La ventaja de la libreria de clases es que permite la abstraccion
	*vec_4bajos = (vec0 * coef0 + vec1 * coef1 + vec2 * coef2 + vec3 * coef3);
	*vec_4altos = (coef3 * vec0 - coef2 * vec1 + coef1 * vec2 - coef0*vec3);

}

// Función wavelet de 4 coeficientes, implementada con extensiones multimedia SSE.
// - Entradas:
//   a: Puntero al vector al que hay que calcularle la transformada.
//   n: Número de elementos del vector de entrada.
//   coef0, ..., coef3: Coeficientes Daub-4.
// - Salidas: Ninguna (la transformada se dejará sobre el mismo
//   vector de entrada).
void wt_4d_SSE(float *a, int n, F32vec4 coef0, F32vec4 coef1, F32vec4 coef2, F32vec4 coef3)
{
  F32vec4 vec0(a[0],a[2],a[4],a[6]);   // 4 empaquetamientos de 4 floats (128 bits cada uno)
  F32vec4 vec1(a[1],a[3],a[5],a[7]);   // para ir guardando...
  F32vec4 vec2(a[2],a[4],a[6],a[8]);   // En la primera pasada,
  F32vec4 vec3(a[3],a[5],a[7],a[9]);   // vec0=(a[0],a[2],a[4],a[6]),
                                   // vec1=(a[1],a[3],a[5],a[7]),
                                   // vec2=(a[2],a[4],a[6],a[8]),
                                   // vec3=(a[3],a[5],a[7],a[9]).
                                   // En la segunda pasada,
                                   // vec0=(a[8],a[10],a[12],a[14]),
                                   // vec1=(a[9],a[11],a[13],a[15]),
                                   // vec2=(a[10],a[12],a[14],a[16]),
                                   // vec3=(a[11],a[15],a[17],a[19]).
                                   // etc.

  F32vec4 vec_4bajos, vec_4altos;   // En cada pasada del próximo "for" se dará lugar
                                   // a 8 floats "transformados"; los 4 primeros floats
                                   // se guardarán en la parte baja del array de resultados,
                                   // y los 4 últimos serán almacenados en la parte alta del
                                   // array de resultados (parte alta=aquella que comienza a
                                   // partir de la mitad del array).
  int i,j,half_n,k;
  float *temp;
  float *aux_coef, aux_c0, aux_c1, aux_c2, aux_c3;


  //temp=(float *) _mm_malloc(n*sizeof(float),32); // Memoria para alojar temporalmente el
                                                 // resultado de la transformada.
  temp=(float *) malloc(n*sizeof(float));

  // Obligatorio: El vector debe tener, al menos, 4 elementos.
  if (n<4) {
    //_mm_free(temp);
    free(temp);
    return;
  }

  if(MOSTRAR)
  {
	printf("\n Antes de entrada");
	for(k=0; k<n; k++) printf("%8.2f", a[k]);
	printf("\n");
  }


  // Bucle principal.
  half_n= n/2;

  //En este caso es necesario declarar las siguientes variables como privadas
  //dado que se usan como arrays temporales de salida de la función
  #pragma omp parallel for num_threads(omp_get_max_threads()) private(vec_4altos, vec_4bajos)
  for (i=0; i<(n-4); i+=8)
  {
    // Instanciación de las entradas (ver declaración de "vec0", ..., "vec3").
    F32vec4 vec0(a[i+6], a[i+4], a[i+2], a[i]);
    F32vec4 vec1(a[i+7], a[i+5], a[i+3], a[i+1]);
    F32vec4 vec2(a[i+8], a[i+6], a[i+4], a[i+2]);
    F32vec4 vec3(a[i+9], a[i+7], a[i+5], a[i+3]);
    // Obtención, a partir de un total de 16 floats de entrada (4 vectores de 4 floats),
    // de 8 "transformados".
    calcular_coeficientes_all(vec0, vec1, vec2, vec3, coef0, coef1, coef2, coef3,
                              &vec_4bajos, &vec_4altos);
    // Almacenamiento de los 8 resultados producidos en la actual pasada.
    storeu(&temp[i/2], vec_4bajos);
    storeu(&temp[half_n+i/2], vec_4altos);
  }

  if(MOSTRAR)
    for (i=0,j=0; i<(n-4); i+=8,j+=4)
    {
      printf("\n Después de salir");
      for(k=0; k<4; k++) printf("%8.2f", temp[j+k]);
      for(k=0; k<4; k++) printf("%8.2f", temp[half_n+j+k]);
      printf("\n");
    }

  // Ajustes finales.
  //aux_coef=(float *) _mm_malloc(4*sizeof(float),32);
  aux_coef=(float *) malloc(4*sizeof(float));
  storeu(&aux_coef[0],coef0);aux_c0=aux_coef[0];
  storeu(&aux_coef[0],coef1);aux_c1=aux_coef[0];
  storeu(&aux_coef[0],coef2);aux_c2=aux_coef[0];
  storeu(&aux_coef[0],coef3);aux_c3=aux_coef[0];
  temp[half_n-1] = aux_c0*a[n-2] + aux_c1*a[n-1] + aux_c2*a[0] + aux_c3*a[1];
  temp[n-1] = aux_c3*a[n-2] - aux_c2*a[n-1] + aux_c1*a[0] - aux_c0*a[1];
  //_mm_free(aux_coef);
  free(aux_coef);

  // Una vez calculada la transformada, la colocamos en el vector de entrada
  // (que pasa, por tanto, a ser el vector de salida).
  for (i=0; i<n; i++) a[i]=temp[i];

  // Liberación de memoria.
  //_mm_free(temp);
  free(temp);
}
