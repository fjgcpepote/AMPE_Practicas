#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
/* Transformada de Wawelet 1D con Daub-4: Versión secuencial. */
// Interpretación de los posibles argumentos que pudiere haber en la invocación
// al programa.
int interpretarArgumentos(int argc, char *argv[], int *n);
// Cálculo de la transformada.
void wt_4d(float *vector, int longVector);

// Función principal.
int main(int argc, char *argv[])
{
int longVector = 1000000, n, respExe = 0, lecturaArgumentos, i;
float *vector, parte_entera, parte_decimal;
struct timeval iteInicio, iteFinal;
double tiempo;
// Interpretación de argumentos (si los hubiere).
lecturaArgumentos = interpretarArgumentos(argc, argv, &n);
if (lecturaArgumentos) {
  if (lecturaArgumentos == 2) { // --help
      printf("Uso: ./wt [--n=<tamaño_vector>]\n");
      printf("Si se indica --n=<tamaño_vector>, el tamaño ha de ser >= 4.\n");
      printf("Si no se indica --n=<tamaño_vector>, el tamaño será %d.\n",longVector);
      printf("Ejemplo 1: ./wt\n");
      printf("Ejemplo 2: ./wt --n=500000\n");
  }
  // La sintaxis de la llamada es correcta, y se puede calcular la transformada.
  else {
    if (lecturaArgumentos == 3) longVector = n; // Tomar valor "n".
    // Inicialización del vector.
    vector = (float*)malloc(sizeof(float)*longVector);
    for (i=0; i<longVector; i++) {
      parte_entera = (float)(rand() % 255);
      parte_decimal = (float)(1 / ((float)(rand() % 100)));
      vector[i] = parte_entera + parte_decimal; // 0 <= vector[i] < 255
    }
    // ¿Mostramos vector de entrada?
    #ifdef MOSTRAR
    printf("ENTRADA:");
    for (i=0; i<longVector; i++) printf("%8.2f ", vector[i]);
    #endif
    // Cálculo de la transformada.
    gettimeofday(&iteInicio, NULL);
    wt_4d(vector, longVector);
    gettimeofday(&iteFinal, NULL);
    tiempo = ((double)iteFinal.tv_sec + (double)iteFinal.tv_usec/1000000) -
      ((double)iteInicio.tv_sec + (double)iteInicio.tv_usec/1000000);
    // ¿Mostramos vector de salida?
    #ifdef MOSTRAR
    printf("\nSALIDA :"); for (i=0; i<longVector; i++) printf("%8.2f ", vector[i]);
    printf("\n");
    #endif
    // Liberación de recursos.
    free(vector);
    // Exhibición del tiempo consumido.
    printf("Tiempo = %6.4f''\n",tiempo);
  }
}
// Error sintáctico.
else {
  printf("Sintaxis incorrecta. Teclee ./wt --help\n");
  respExe = 1;
}

return respExe;

}

// Interpretación de los posibles argumentos que pudiere haber en la invocación
// al programa.
// - Entradas:
// argc: Número de argumentos de entrada.
// argv: Puntero a los argumentos de entrada.
// n: Puntero a una variable en donde se dejará la longitud del vector al que
// se le calculará la Transformada de Wavelet.
// - Salidas:
// 0 -> Sintaxis errónea. 1 -> Sin argumentos. 2 -> Argumento "--help".
// 3 -> Argumento "--n="; en este caso, el valor indicado se dejará sobre "n".
int interpretarArgumentos(int argc, char *argv[], int *n)
{
int resp = (argc == 1 || argc == 2); // Si el programa no tiene argumentos (argc=1)
if (resp && argc == 2) // o si tiene uno solo (argc=2), todo ok.
{
// Hay un argumento. Éste podrá ser "--help" o "--n=<tamaño_vector>".
if (strcmp(argv[1],"--help") == 0) resp = 2;
else
if (strncmp(argv[1],"--n=",4) == 0)
{
*n = atoi(&argv[1][4]);


if (*n >= 4) resp = 3;
else resp = 0;
}
else resp = 0;
}
return resp;
}
// Cálculo de la transformada.
void wt_4d(float *vector,int longVector)
{
// Coeficientes wavelets Daub-4.
float c0 = 0.4829629131445341;
float c1 = 0.8365163037378079;
float c2 = 0.2241438680420134;
float c3 = -0.1294095225512604;
// Otras variables.
int i, j, mitad;
float *tmp;

// Control inicial: El vector de entrada debe contar con, al menos, 4 elementos.
if (longVector >= 4)
{ tmp = (float *) malloc(sizeof(float)*longVector); // Reserva del espacio necesario.
// Bucle wavelet.
mitad = longVector / 2;
for (i = 0, j = 0; i < longVector - 3; i += 2, j++)
{
	tmp[j] = c0 * vector[i] + c1 * vector[i+1] + c2 * vector[i+2] + c3 * vector[i+3];
	tmp[j+mitad] = c3 * vector[i] - c2 * vector[i+1] + c1 * vector[i+2] - c0 * vector[i+3];
}
// Ajustes finales.
tmp[j] = c0 * vector[longVector-2] + c1 * vector[longVector-1] + c2 * vector[0] + c3 * vector[1];
tmp[j+mitad] = c3 * vector[longVector-2] - c2 * vector[longVector-1] + c1 * vector[0] - c0 * vector[1];
// Paso de los resultados, del vector temporal al vector de entrada/salida.
for (i = 0; i < longVector; i++) vector[i] = tmp[i];
free(tmp); // Liberación del espacio ya utilizado.
}
}
